#ifndef LinkedList_H_
#define LinkedList_H_

#include "node.h"

template <class Type>
class LinkedList {
    public:
        const LinkedList<Type>& operator= (const LinkedList<Type>&);
        LinkedList();
        LinkedList(const LinkedList<Type>&);
        ~LinkedList();
        Type front() const;
        Type back() const;
        void push_front(const Type &);
        void push_back(const Type &);
        void pop_front();
        void pop_back();
        bool empty() const;
        int size() const;
        void print() const;
        void copyList(const LinkedList<Type> &);
    protected:
        node<Type> *head;
        node<Type> *tail;
        int count;
        void destroyList();
};

#include "LinkedList_impl.h"

#endif
