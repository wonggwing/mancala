#ifndef LinkedList_impl_H_
#define LinkedList_impl_H_

#include "LinkedList.h"
#include "assert.h"
#include <iostream>
#include <iomanip>
using namespace std;

template <class Type>
LinkedList<Type>::LinkedList() {
    //default constructor
    head = NULL;
    tail = NULL;
    count = 0;
}

template <class Type>
Type LinkedList<Type>::front() const {
    if ( head != NULL ){
        return head->data;
    }else{
        return NULL;
    }
}

template <class Type>
Type LinkedList<Type>::back() const {
    if ( tail != NULL ){
        return tail->data;
    }else{
        return NULL;
    }
}

template <class Type>
void LinkedList<Type>::push_front(const Type& item) {
    node<Type> *newNode = new node<Type>;
    newNode->data = item;
    newNode->next = head;
		if (empty())
            head = tail = newNode;
        else
            head = newNode;
        count++;
    //increment count
}

template <class Type>
void LinkedList<Type>::push_back(const Type& item) {
    node<Type> *newNode = new node<Type>;
    newNode->data= item;
    newNode->next= NULL;
    if (empty())
        head = tail = newNode;
    else {
        tail->next = newNode;
        tail = newNode;
    }
    count++;
    //increment count
}

template <class Type>
void LinkedList<Type>::pop_front() {
    assert(!empty());
    // stops program if parameter is false
    node<Type> *temp = head;
    if (count==1)
        head = tail = NULL;
    else
        head = head->next;
    count--;
    //decrement count
}

template <class Type>
void LinkedList<Type>::pop_back() {
    assert(!empty());
    // stops program if parameter is false
    if (count==1)
        head = tail = NULL;
    else {
        node<Type> *current = head;
    while (current->next != tail) {
        current = current->next;
    }
    tail = current;
    delete current->next;
    tail->next = NULL;
    }
    count--;
    //decrement count
}

template <class Type>
bool LinkedList<Type>::empty() const {
    return count==0;
}
template <class Type>
int LinkedList<Type>::size() const {
    return count;
}

template <class Type>
void LinkedList<Type>::print() const {
    cout << "[ ";
    node<Type> *current;
    //pointer for traversal
    current = head;
    //set current to point to head
    while (current != NULL) {
        // loop till end of list
        cout << current->data << " ";
        current = current->next;
    }
    cout << "]";
}

template <class Type>
void LinkedList<Type>::destroyList() {
    node<Type> *temp;
    // pointer for deallocation
    while (head != NULL) {
        temp = head;
        head = head->next;
        delete temp;
    }
    tail = NULL;
    // set last to NULL;
    count = 0;
}

template <class Type>
LinkedList<Type>::~LinkedList() {
    destroyList();
}

template <class Type>
void LinkedList<Type>::copyList (const LinkedList<Type>& otherList) {
    node<Type> *newNode;
    //pointer to create a node
    node<Type> *current;
    //pointer to traverse the list
    if (head != NULL) destroyList();
    // empty the list first
    if (otherList.head == NULL) {
        //otherList is empty
        head = tail = NULL;
        count = 0;
    } else {
        current = otherList.head;
        //current points to list head
        count = otherList.count; // copy list length
        head = new node<Type>;  //create the node
        head->data= current->data; //copy the data
        head->next = NULL; //set next node to NULL
        tail = head; //set last node to current node
        current = current->next; // move current to next node
        while (current != NULL) { // other nodes in list
            newNode = new node<Type>;  //create a node
            newNode->data = current->data; //copy the data
            newNode->next = NULL; //set next node to NULL
            tail->next = newNode;  //attach newNode at the end
            tail = newNode; //make tail to point to last node
            current = current->next; //move current to next node
        }//end while
    }//end else
}//end copyList

template <class Type>
LinkedList<Type>::LinkedList(const LinkedList<Type>& otherList) {
    head = NULL;
    copyList(otherList);
}

template <class Type> const LinkedList<Type>& LinkedList<Type>::operator= (const LinkedList<Type>& otherList) {
    if (this != &otherList) copyList(otherList);
    return *this;
}

#endif
