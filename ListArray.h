#ifndef ListArray_H_
#define ListArray_H_

#include "LinkedList.h"
#include "node.h"

template <class Type>
class ListArray : public LinkedList<Type> {
    public:
        void init(int count);
        void set_data(int index, const Type &);
        Type get_data(int index) const;
        Type get_front() const;
        Type get_back() const;
};

#include "ListArray_impl.h"

#endif
