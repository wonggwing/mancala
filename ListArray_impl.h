#ifndef ListArray_impl_H_
#define ListArray_impl_H_

#include "LinkedList.h"
#include "ListArray.h"
#include <iostream>
#include <iomanip>
using namespace std;

template <class Type>
void ListArray<Type>::init(int count){
    int i = 0;
    if ( ListArray<Type>::size() < count )
    {
        while ( i < count )
        {
            ListArray<Type>::push_back(0);
            i++;
        }
    }
}

template <class Type>
void ListArray<Type>::set_data(int index, const Type & data){
    node<Type> *current = ListArray<Type>::head;
    int i = 0;
    while (current != NULL) {
        if ( i == index )
        {
            current->data = data;
            break;
        }
        // loop through the end
        // may do something here
        current = current->next;
        i++;
    }
}

template <class Type>
Type ListArray<Type>::get_data(int index) const{
    node<Type> *current = ListArray<Type>::head;
    int i = 0;
    while (current != NULL) {
        if ( i == index )
        {
            return current->data;
        }
        // loop through the end
        // may do something here
        current = current->next;
        i++;
    }
    return 0;
}

// Unsafe: check size before use
// Return dynamic class type cannot return NULL
template <class Type>
Type ListArray<Type>::get_front() const {
    return ListArray<Type>::head->data;
}

// Unsafe: check size before use
// Return dynamic class type cannot return NULL
template <class Type>
Type ListArray<Type>::get_back() const {
    return ListArray<Type>::tail->data;
}

#endif
