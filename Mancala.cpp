#include "Mancala.h"
#include <iostream>
#include <iomanip>
#include <cstdlib>
using namespace std;

Mancala::Mancala (int pit, int seed) {
    maxPit = pit;
    row[0].init(maxPit);
    row[1].init(maxPit);

    for ( int i = 0 ; i < maxPit ; i++ )
    {
        setSeed(0, i + 1, seed);
        setSeed(1, i + 1, seed);
    }

    store.init(2);
    setScore(0, 0);
    setScore(1, 0);

    playerTurn = 0;
    /*
    for ( int i = 0 ; i < 2 ; i++ )
    {
        for ( int j = 0 ; j < maxPit ; j++ )
        {
            cout << "row " << i << ": " << j << ": " << row[i][j] << endl;
        }
    }
    */
}

bool Mancala::move(int player, int playerPit) {
    bool hasExtraMove = false;

    if ( checked )
    {
	    // Copy row status
	    ListArray<int> rowStatus0;
	    ListArray<int> rowStatus1;

        rowStatus0.copyList(row[0]);
        rowStatus1.copyList(row[1]);

	    rowUndo[0].push_back(rowStatus0);
	    rowUndo[1].push_back(rowStatus1);

	    // Copy store status
	    ListArray<int> rowStoreStatus;

        rowStoreStatus.copyList(store);

	    storeUndo.push_back(rowStoreStatus);

	    // Copy player turn
	    int playerTurnStatus;

        playerTurnStatus = playerTurn;

	    playerTurnUndo.push_back(playerTurnStatus);

	    int currentPlayer = player;
	    int currentPit = playerPit;

	    int seedCount = pit(currentPlayer, currentPit);
	    setSeed(currentPlayer, currentPit, 0);
	    while ( seedCount > 0 )
	    {
	        currentPit++;
	        if ( currentPit <= maxPit )
	        {
	            setSeed(currentPlayer, currentPit, pit(currentPlayer, currentPit) + 1);
	            seedCount--;
	        } else {
	            if ( currentPlayer == player )
	            {
	                setScore(player, score(player) + 1);
	                seedCount--;

	                currentPit = 0;
	                currentPlayer = 1 - currentPlayer;
	            } else {
	                // opponent pit
	                currentPit = 0;
	                currentPlayer = 1 - currentPlayer;
	            }
	        }
	    }

	    // currentPlayer, currentPit is the last position sowed
	    if ( currentPit != 0 )
	    {
	        int opponentPlayer = 1 - currentPlayer;
	        int opponentPit = maxPit - currentPit + 1;
	        int myPitSeed = pit(currentPlayer, currentPit);
	        int opponentPitSeed = pit(opponentPlayer, opponentPit);

	        if ( currentPlayer == player && pit(currentPlayer, currentPit) == 1 && opponentPitSeed > 0 )
	        {
	            setScore(currentPlayer, score(currentPlayer) + opponentPitSeed + myPitSeed);
	            setSeed(currentPlayer, currentPit, 0);
	            setSeed(opponentPlayer, opponentPit, 0);
	        }

	        // Change player
	        playerTurn = 1 - playerTurn;
	    } else {
	        // Extra move ONLY if currentPlayer == player
	        hasExtraMove = true;
	    }
	}

    checked = false;
    return hasExtraMove;
}
bool Mancala::check(int player, int playerPit){
	checked = false;

	if ( playerPit >= 1 && playerPit <= maxPit && pit(player, playerPit) > 0 ){
		checked = true;
	}

	return checked;
}
int Mancala::score(int player) const {
    return store.get_data(player);
}
void Mancala::setScore(int player, int score) {
	store.set_data(player, score);
}
int Mancala::pit(int player, int pit) const {
    int seedCount = 0;
    if ( player == 0 ){
        seedCount = row[player].get_data(pit - 1);
    }
    else if ( player == 1 ){
        seedCount = row[player].get_data(maxPit - pit);
    }
    return seedCount;
}
void Mancala::setSeed(int player, int pit, int seed) {
    if ( player == 0 ){
        row[player].set_data(pit - 1, seed);
    }
    if ( player == 1 ){
        row[player].set_data(maxPit - pit, seed);
    }
}
int Mancala::getPlayer() const{
    return playerTurn;
}
void Mancala::printBoard() const {
    cout << "======================================================================" << endl;

    // Board row 1
    cout << "                  ";
    for ( int i = 1 ; i <= maxPit ; i++ )
    {
        cout << maxPit - i + 1;
        if ( i < maxPit ){
            cout << "<";
        }
    }
    cout << endl;

    // Board Row 2
    cout << "Player 1 [[" << std::setw(2) << std::setfill(' ') << score(1) << "]] [ ";
    for ( int j = 1 ; j <= maxPit ; j++ )
    {
        cout << pit(1, maxPit - j + 1);
        if ( j < maxPit ){
            cout << " ";
        }
    }
    cout << " ]";
    cout << endl;

    // Board Row 3
    cout << "                [ ";
    for ( int j = 1 ; j <= maxPit ; j++ )
    {
        cout << pit(0, j);
        if ( j < maxPit ){
            cout << " ";
        }
    }
    cout << " ] [[" << std::setw(2) << std::setfill(' ') << score(0);
    cout << "]] Player 0";
    cout << endl;

    // Board row 4
    cout << "                  ";
    for ( int i = 1 ; i <= maxPit ; i++ )
    {
        cout << i;
        if ( i < maxPit ){
            cout << ">";
        }
    }
    cout << endl;

    cout << "======================================================================" << endl;
}
bool Mancala::gameEnd() {
    bool isGameEnd;
    // if any row become empty

	// Check for player 0
    bool isEmpty = true;
    for ( int i = 1 ; i <= maxPit ; i++ )
    {
        if ( pit(0,i) > 0 ){
        	isEmpty = false;
		}
    }
    if ( isEmpty )
    {
    	// end game
    	isGameEnd = true;
	} else {
		// Check again for player 1
		isEmpty = true;
	    for ( int i = 1 ; i <= maxPit ; i++ )
	    {
	        if ( pit(1,i) > 0 ){
	        	isEmpty = false;
			}
	    }

	    if ( isEmpty )
	    {
	    	// end game
	    	isGameEnd = true;
		}
	}

    // clear all puts of oppenent and put the stones into its store
    if ( isGameEnd )
    {
        for ( int i = 1 ; i <= maxPit ; i++ )
        {
            setScore(0, score(0) + pit(0,i));
            setSeed(0, i, 0);

            setScore(1, score(1) + pit(0,i));
            setSeed(1, i, 0);
        }
    }

    return isGameEnd;
}

// Try to undo, if there is a stack available.
bool Mancala::undo() {
    bool undoDone = false;
    if ( storeUndo.size() > 0 ){
        // Restore row
        ListArray<int> rowStatus0 = rowUndo[0].get_back();
        ListArray<int> rowStatus1 = rowUndo[1].get_back();

        rowUndo[0].pop_back();
        rowUndo[1].pop_back();

        row[0] = rowStatus0;
        row[1] = rowStatus1;

        // Restore store
        ListArray<int> storeStatus = storeUndo.get_back();

        storeUndo.pop_back();

        store = storeStatus;

        // Restore player
        int playerTurnStatus = playerTurnUndo.get_back();

        playerTurnUndo.pop_back();

        playerTurn = playerTurnStatus;

        undoDone = true;
    }
    return undoDone;
}
