//Header file for Mancala Class
//This File is interface file which will not compiled by compiler
//Reference file for cpp file use

#include <iostream>
#include <iomanip>
#include "ListArray.h"
using namespace std;

class Mancala {
        ListArray<int> row[2];
        ListArray< ListArray<int> > rowUndo[2];
        ListArray<int> store;
        ListArray< ListArray<int> > storeUndo;
        int playerTurn;
        ListArray<int> playerTurnUndo;
        int maxPit;
        bool checked;
public:
        Mancala(int pit=6, int seed=4);
        bool move(int player, int pit);
        int score(int player) const;
        int pit(int player, int pit) const;
        int getPlayer() const;
        void printBoard() const;
        bool gameEnd();
        bool check(int player, int playerPit);
        bool undo();
private:
        void setSeed(int player, int pit, int seed);
		void setScore(int player, int score);
};
