#include "ListArray.h"
#include "Mancala.h"
#include <iostream>
#include <iomanip>
#include <cstdlib>
using namespace std;

int main() {
        int maxPit, seed;
        cout << "Welcome to Mancala game!" << endl;
        cout << "========================" << endl;

        // INPUT DATA
        cout << "How many pits in each row? ";
        cin >> maxPit;
        if (maxPit < 4) maxPit = 4;

        cout << "How many stones in each pit initially? ";
        cin >> seed;
        if (seed < 3) seed = 3;

        // SET UP OBJECT
        Mancala game(maxPit, seed);

        // PLAY GAME
        int player = 0;
        bool extraMove = false;
        bool undoDone = false;
        int pit;

    	game.printBoard();
        while (!game.gameEnd()) {
            player = game.getPlayer();
            cout << "Player " << player << ", which pit do you want to pick (1-" << maxPit << ")? (-1 = undo) ";
            cin >> pit;

            if ( cin.good() )
            {
                if ( game.check(player, pit) )
                {
                    extraMove = game.move(player, pit);
                    if ( extraMove )
                    {
                        cout << "Player " << player << " earns an extra move!!" << endl;
                    }
                }
                else if ( pit == -1 )
                {
                    // Undo
                    undoDone = game.undo();
                    if ( undoDone ){
                        cout << "Undo to last move.. " << endl;
                    } else {
                        cout << "Cannot undo, no more previous moves" << endl;
                    }
                } else {
                    cout << "This is not a valid pit!!" << endl;
                    cin.clear();
                    cin.ignore(INT_MAX, '\n');
                }
			} else {
				cout << "This is not a valid pit!!" << endl;
				cin.clear();
				cin.ignore(INT_MAX, '\n');
			}

            game.printBoard();
        }

        // game END  PRINT RESULT
        game.printBoard();
        cout << "Game ends.";
        if (game.score(0) > game.score(1))
                cout << " Player 0 wins!!";
        else if (game.score(0) < game.score(1))
                cout << " Player 1 wins!!";
        else cout << " Draw!";
        cout << endl;

        system("pause");
        return 0;
}
