#ifndef NODE_H_
#define NODE_H_

template <class Type>
struct node {
    Type data;
    node<Type> *next;
};

#endif
